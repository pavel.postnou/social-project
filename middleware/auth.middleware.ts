import { Request, Response, NextFunction } from 'express'
import * as jwt from 'jsonwebtoken';
import {accessToken} from '../config/token';
import {UserInterface} from '../models/userModel';

interface CustomRequest<T> extends Request {
  user:any
}


const verifyToken = (req:CustomRequest<UserInterface>, res:Response, next:NextFunction) => {
    if(req.headers.authorization) {       
        const token = req.headers.authorization.split(' ')[1]; 
  if (!token) {
    return res.status(403).send("A token is required for authentication");
  }
  try {
    jwt.verify(token, accessToken, (err,user) => {
      if (err) {
        return res.status(401).send("Invalid Token");
      }

      req.user = user as UserInterface
      return next(); 
    });
  } catch (err) {
    return res.status(401).send("Invalid Token");
  }
  
};
}
export default verifyToken; 
