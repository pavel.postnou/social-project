const stripe = require("stripe")(
  "sk_test_51JwmByBCGSa6irvEAw27KnsRMxGLJaIPkDnGy64epU3LYJgU2TqpuDajg9Pj6nwznb2mnrWKTNRZ8E5xeAvdMYkI00lyxGheZo"
);
import express from "express";
const pay = async (
  req: express.Request,
  res: express.Response
): Promise<void> => {
  let str = req.body.amount;
  let email = req.body.email;
  str.length == 1 ? (str = str + ".00") : null;
  console.log(email);
  let am = Number.parseInt(str.replace(/\./g, ""));
  const customers = await stripe.customers.list({
    email: email,
  });
  console.log(customers);
  if (customers.data.length > 0) {
    const ephemeralKey = await stripe.ephemeralKeys.create(
      { customer: customers.data[0].id },
      { apiVersion: "2020-08-27" }
    );
    const paymentIntent = await stripe.paymentIntents.create({
      amount: am,
      currency: "usd",
      customer: customers.data[0].id,
      automatic_payment_methods: {
        enabled: true,
      },
    });

    res.json({
      paymentIntent: paymentIntent.client_secret,
      ephemeralKey: ephemeralKey.secret,
      customer: customers.data[0].id,
      publishableKey:
        "pk_test_51JwmByBCGSa6irvE39b37uhBBkExVnqVeYm9Ouq40RwTDpLP5IrTbgNi4PwB4oQOETCEYOVHhbyRQvBGHZSxZ0iJ00ziBSEmph",
    });
  } else {
    const customer = await stripe.customers.create({
      email: email,
    });
    const ephemeralKey = await stripe.ephemeralKeys.create(
      { customer: customer.id },
      { apiVersion: "2020-08-27" }
    );
    const paymentIntent = await stripe.paymentIntents.create({
      amount: am,
      currency: "usd",
      customer: customer.id,
      automatic_payment_methods: {
        enabled: true,
      },
    });

    res.json({
      paymentIntent: paymentIntent.client_secret,
      ephemeralKey: ephemeralKey.secret,
      customer: customer.id,
      publishableKey:
        "pk_test_51JwmByBCGSa6irvE39b37uhBBkExVnqVeYm9Ouq40RwTDpLP5IrTbgNi4PwB4oQOETCEYOVHhbyRQvBGHZSxZ0iJ00ziBSEmph",
    });
  }
};

const subscribe = async (req: express.Request, res: express.Response) => {
 let bill = req.body.amount;
 bill.length == 1 ? (bill = bill + ".00") : null;
  let am = Number.parseInt(bill.replace(/\./g, ""));
 let email = req.body.email;

  const product = await stripe.products.create({
    name: 'Monthly removed ads',
    type: 'service',
  });

  const price = await stripe.prices.create({
    nickname: 'Standard Monthly',
    product: product.id,
    unit_amount: am,
    currency: 'usd',
    recurring: {
      interval: 'month',
      usage_type: 'licensed',
    },
  });
  
  const customers = await stripe.customers.list({
    email: email,
  });
  const customerId = customers.data[0].id
 
  const paymentMethods = await stripe.customers.listPaymentMethods(
  customerId,
  {type: 'card'}
);
console.log(paymentMethods)
  try {
    console.log("subscribe")
    const subscription = await stripe.subscriptions.create({
      customer: customerId,
      items: [{
        price: price.id,
      }],
      payment_behavior: 'error_if_incomplete',
      expand: ['latest_invoice.payment_intent'],
    });

    console.log(subscription.id)
    console.log(subscription)

    res.status(200).send()
  } catch (error:any) {
    console.log({ error: { message: error.message } })
    return res.status(400).send({ error: { message: error.message } });
  }
};
export {pay, subscribe};
