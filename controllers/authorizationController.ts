import { User } from "../models/userModel";
import { Request, Response } from "express";
import * as jwt from "jsonwebtoken";
import { accessToken } from "../config/token";
import * as bcrypt from "bcrypt";

const regUser = async (req: Request, res: Response) => {
  try {
    const { firstName, lastName, email, password } = req.body;
    if (!(email && password && lastName && firstName)) {
      res.status(400).json("All input is required");
    }
    const oldUser = await User.findOne({ email });
    if (oldUser) {
      return res.status(409).json("User with this email already exist");
    }
    let encryptedPassword = await bcrypt.hash(password, 10);
    const user = await User.create({
      firstName,
      lastName,
      email: email.toLowerCase(),
      password: encryptedPassword,
    });
    res.status(200).json(user);
  } catch (e) {
    res.status(400).json(e);
  }
};

const loginUser = async (req: Request, res: Response) => {
  try {
    const { email, password } = req.body;
    if (!(email && password)) {
      res.status(402).json("All input is required");
    }
    const user = await User.findOne({ email });
    if (user && (await bcrypt.compare(password, user.password))) {
      const token = jwt.sign({ user_id: user._id, email }, accessToken, {
        expiresIn: "2h",
      });
      res.status(200).json({ user: user, accessToken: token });
    }
    else {
    res.status(401).json("Invalid Credentials");
  }
  } catch (e) {
    res.status(400).json(e);  
  }
};

export { regUser, loginUser };