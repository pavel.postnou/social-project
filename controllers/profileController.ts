
import {User} from '../models/userModel'
import { Request, Response } from 'express'


  const changeAvatar = async (req: Request ,res: Response) => {
    try {
      const userId = req.body.userId
      const newAvatar = req.body.newAvatar
      await User.updateOne({_id: userId}, {avatar: newAvatar});
      res.status(200).json("true")
    } catch(e) {
      res.status(400).json(e)
    }
  };

export {changeAvatar};