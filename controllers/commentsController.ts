import { Post } from '../models/postModel'
import { Comment } from '../models/commentModel'
import { Request, Response } from 'express'

const addNewComment = async (req: Request, res: Response) => {
  try {
    const { authorId, firstName, lastName, avatar, text, postId, date } = req.body;

    const post = await Post.findById(postId);
    const com = await Comment.create({
      authorId,
      firstName,
      lastName,
      avatar,
      text,
      date,
      postId
    })
    post?.comments.push(com._id)
    await post?.save();
    res.status(200).json("true")
  } catch (e) {
    res.json(e)
  }
} 

const deleteComment = async (req: Request, res: Response) => {
  try {
    const commentId = req.params.id
    const comment = await Comment.findById(commentId);
    await Comment.findByIdAndDelete(commentId);
    await Post.findByIdAndUpdate(comment?.postId,{$pull: {comments:commentId}})
    res.status(200).json("true")
  } catch (e) {
    res.status(400).json(e)
  }
} 

const getComments = async(req: Request, res: Response) => {
  try {
  const post = await Post.findById(req.params.id);
  post? (res.status(200).json({post}).send):null
  } catch (e) {
    res.status(400).json(e)
  }
}



export { addNewComment,getComments,deleteComment }