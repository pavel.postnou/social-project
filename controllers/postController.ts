import {Post} from '../models/postModel'
import {User} from '../models/userModel'
import {Comment} from '../models/commentModel'
import { Request, Response } from 'express'

  
  const getAllUserPostsById = async (req: Request ,res: Response) => {
    try {
      const user = await User.findById({_id: req.params.id});
      if (user) {
      res.status(200).json(user.posts)
      }
    } catch(e) {      
      res.status(400).json(e)
    }
  }
  
  const addNewPost = async (req: Request ,res: Response) => {
    try {
      const { title, text, picture, owner, video, date } = req.body;
      const post = await Post.create({
        title,
        text, 
        picture, 
        owner,
        video,
        date 
      }); 
      const user = await User.findById(owner);
      if(user) {
      user.posts.push(post._id)
      await user.save()
      }
      res.status(200).json(post)
    } catch(e) {
      res.status(400).json(e)
    }    
  } 

  const likePostById = async (req: Request ,res: Response) => {
    try {   
      const post = await Post.findOne({_id: req.body.postId});
      if (post){
      const isAlreadyLiked = post.likes.indexOf(req.body.userId);
      if(isAlreadyLiked === -1) {
        await Post.updateOne({_id: req.body.postId}, {likes: [...post.likes, req.body.userId]})
      } else {
        await Post.updateOne({_id: req.body.postId}, {likes: post.likes.filter((item: any) => item != req.body.userId)})
      }    
      res.status(200).json(post)
      } 
    } catch(e) {
      res.status(400).json(e)
    }
  }

 const deletePost = async (req: Request ,res: Response) => {
  try {   
    const postId = req.params.id
    const post = await Post.findOne({_id: req.params.id});
    
    if (post){
      const userId = post.owner
      await Post.findByIdAndDelete(postId)
      await User.findByIdAndUpdate(userId,{$pull: {posts:postId}})
      await Comment.deleteMany({postId:postId})
    }
    res.status(200).json("true")
  } catch(e) {
    res.status(400).json(e)
  }
}


export{addNewPost,getAllUserPostsById,likePostById, deletePost};