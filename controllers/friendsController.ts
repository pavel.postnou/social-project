import { User } from '../models/userModel'
import { Request, Response } from 'express'
import {Chat} from '../models/chatModel'

const askNewFriend = async (req: Request, res: Response) => {
  try {
    const {myId, friendId} = req.body;
    const friend = await User.findById(friendId);
    const me = await User.findById(myId);

    if (friend && me) {
      friend.invitations.push(myId)
      await friend.save();
      res.status(200).json("")
    }
  } catch (e) {
    res.status(400).json(e)
  }
}

const declineNewFriend = async (req: Request, res: Response) => {
  try {
    const { myId, friendId } = req.body;
    const friend = await User.findById(friendId);
    const me = await User.findById(myId);
    if (friend && me) {
      await User.updateOne({ _id: myId }, { $pull: { invitations: friendId } })
      res.status(200).json("")
    }
  } catch (e) {
    res.status(400).json(e)
  }
}

const showUsers = async (req: Request, res: Response) => {
  try {
    const users = await User.find();
    if (users) {
      res.status(200).json({ users })
    }
  } catch (e) {
    res.status(400).json(e)
  }
}

const getUserInfo = async (req: Request, res: Response) => {
  try {
    const userId = req.params.id;
    const user = await User.findById(userId);
    if (user) {
      res.status(200).json({ user })
    }
  } catch (e) {
    res.status(400).json(e)
  }
}

const addNewFriend = async (req: Request, res: Response) => {
  try {
    const { myId, friendId } = req.body;
    const me = await User.findById(myId);
    const friend = await User.findById(friendId);
    if (me && friend) {
      me.friends.push(friendId)
      await me.save();
      friend.friends.push(myId)
      await friend.save();
      await User.updateOne({ _id: myId }, { $pull: { invitations: friendId } })
      await User.updateOne({ _id: friendId }, { $pull: { invitations: myId } })
      const chat = await Chat.findOne({ usersId: { $all: [myId, friendId] } })
      if (!chat){
      await Chat.create({
        usersId:[myId,friendId],
        messages:[]
      }); 
    }
      res.status(200).json("")
    }
  } catch (e) {
    res.status(400).json(e)
  }
}

const deleteFriend = async (req: Request, res: Response) => {
  try {
    const { myId, friendId } = req.body;
    await User.updateOne({ _id: myId }, { $pull: { friends: friendId } })
    await User.updateOne({ _id: friendId }, { $pull: { friends: myId } })
    res.status(200).json("")
  }
  catch (e) {
    res.status(400).json(e)
  }
}




export { askNewFriend, showUsers, getUserInfo, addNewFriend, deleteFriend, declineNewFriend };