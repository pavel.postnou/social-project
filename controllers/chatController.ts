import { Chat } from '../models/chatModel'
import { Message} from '../models/messageModel';
import { Request, Response } from 'express'

const getChat = async (req: Request, res: Response) => {
    try {
        const { myId, friendId } = req.body;
        const chat = await Chat.findOne({ usersId: { $all: [myId, friendId] } })
        const messages = await Message.find({ _id: chat?.messages } )
        res.status(200).json({chat, messages})
    } catch (e) {
        res.status(400).json(e)
    }
};

const sendMessageToChat = async (mes:any) => {
    try {
        const message = await Message.create({
            author:mes.myId, 
            text:mes.mes,
            date:Date.now()
          }); 
        const chat = await Chat.findById(mes.chatId)
        chat?.messages.push(message._id)
        await chat?.save()
        const messages = await Message.find({ _id: chat?.messages } )
        const chatId = chat?._id
        return({messages,chatId}) 
 
    } catch (e) { 
        return(e)
    }
};

export { getChat, sendMessageToChat } 