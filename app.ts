import mongoose from "mongoose";
import * as bodyParser from 'body-parser';
import verifyToken from "./middleware/auth.middleware";

const express = (require('express'));
const app = express();
const urlencodedParser = bodyParser.urlencoded({ extended: true });

mongoose.connect("mongodb://localhost:27017/social",
    {
        useUnifiedTopology: true,
        useNewUrlParser: true,
        useFindAndModify: false,
        autoIndex:false
    })
    .then(() => {
        console.log("Successfully connected to database");
    })
    .catch((error: string) => {
        console.log("database connection failed. exiting now...");
        console.error(error);
        process.exit(1);
    });

import { authRouter } from './routes/authorizationRouter';
import { postRouter } from './routes/postRouter';
import { friendsRouter } from './routes/friendsRouter';
import { profileRouter } from './routes/profileRouter';
import { commentsRouter } from './routes/commentsRouter';
import { chatRouter } from './routes/chatRouter';
import {paymentRouter} from './routes/paymentRouter';
 
app.use('', bodyParser.json(), authRouter);
app.use("/post", bodyParser.json(), verifyToken,  postRouter)
app.use("/comments", bodyParser.json(),verifyToken, commentsRouter)
app.use("/users", bodyParser.json(),verifyToken, friendsRouter)
app.use("/profile", bodyParser.json(),verifyToken, profileRouter)
app.use("/chats", bodyParser.json(),verifyToken, chatRouter)
app.use('/payment',paymentRouter)

const PORT = 3000;
const socket = require("socket.io");
const server = app.listen(PORT, () => {
    console.log(`Trying to listen port ${PORT}.`);
})

import {sendMessageToChat} from './controllers/chatController'

const io:any = socket(server)

io.on("connection", (socket: any) => {
    console.log(`Zarabotalo`);
    socket.on('sendMessage', async (message:any) => {
        const messages = await sendMessageToChat(message)
        const oldChatId = message.chatId
        io.emit('tweet', {messages, oldChatId});
    })
    socket.on('disconnecting', () => {
        console.log("otklychili") 
      });
})
