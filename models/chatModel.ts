import mongoose from 'mongoose'
const Schema = mongoose.Schema;

interface ChatInterface {
 usersId:[mongoose.Types.ObjectId],
 messages:[mongoose.Types.ObjectId]
}

const ChatSchema = new Schema({
    usersId: [{type: mongoose.Types.ObjectId, required: true, ref: 'User'}], 
    messages: [{type: mongoose.Types.ObjectId, required: true, ref: 'Message', default:[]}]
});
 
ChatSchema.plugin(require('mongoose-autopopulate'));
const Chat = mongoose.model<ChatInterface>("Chat", ChatSchema);
export {Chat, ChatInterface}; 