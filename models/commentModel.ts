import mongoose from 'mongoose'
const Schema = mongoose.Schema;

interface CommentInterface {
  authorId:string,
  firstName:string,
  lastName:string,
  avatar:string,
  date:Date,
  text:string,
  postId:string
} 

const CommentSchema = new Schema({
  authorId: {type: String, required: true},
  firstName: {type: String, required: true},
  lastName: {type: String, required: true},
  avatar: {type: String,required: true},
  date: {type: Date, default: Date.now()},
  text: {type: String, required: true},
  postId:{type: String, required: true}
});

CommentSchema.plugin(require('mongoose-autopopulate'));
const Comment = mongoose.model<CommentInterface>("Comment", CommentSchema);
export {Comment, CommentInterface};   