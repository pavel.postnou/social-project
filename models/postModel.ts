import mongoose from 'mongoose'
const Schema = mongoose.Schema;

interface PostInterface {
    title:string,
    text:string,
    picture:string,
    video:string,
    date:Date,
    likes:[mongoose.Types.ObjectId],
    owner:mongoose.Types.ObjectId,
    comments:[mongoose.Types.ObjectId]

}
const PostSchema = new Schema({
  title: {type: String, required: true},
  text: {type: String, required: true},
  picture: {type: String, default:null},
  video: {type: String},
  date: {type: Date, default: Date.now()},
  likes: [{type: mongoose.Types.ObjectId, ref: 'User'}],
  owner: {type: mongoose.Types.ObjectId, required: true, ref: 'User', autopopulate:{ maxDepth: 1 }},
  comments: [{type: mongoose.Types.ObjectId, required: false, ref:'Comment', autopopulate:{ maxDepth: 1}}] 
});
 
PostSchema.plugin(require('mongoose-autopopulate'));
const Post = mongoose.model<PostInterface>("Post", PostSchema);
export {Post, PostInterface};  