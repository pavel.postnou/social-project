import mongoose from 'mongoose'
const Schema = mongoose.Schema;

interface IMessage {
    text: string,
    author:string,
    date:Date
   }

const MessageSchema = new Schema({
  text: {type: String, required: true},
  author: {type: mongoose.Types.ObjectId, required: true, ref: 'User',  autopopulate:{ maxDepth: 1 }},
  date: {type: Date, default: Date.now()},
});

MessageSchema.plugin(require('mongoose-autopopulate'));
const Message = mongoose.model<IMessage>("Message", MessageSchema);
export {Message, IMessage}; 