import mongoose from 'mongoose'
const Schema = mongoose.Schema;

interface UserInterface extends mongoose.Document {
  firstName: string;
  lastName:string;
  email:string;
  password:string;
  avatar:string;
  friends:[mongoose.Types.ObjectId]
  invitations:[mongoose.Types.ObjectId]
  posts:[mongoose.Types.ObjectId]
}

  const UserSchema = new Schema({
    firstName: {type: String, required:true},
    lastName: {type: String, required:true},
    email: {type: String, unique: true},
    password: { type: String, required: true },
    avatar: {type: String, default:"https://pristor.ru/wp-content/uploads/2019/09/%D0%A1%D0%BA%D0%B0%D1%87%D0%B0%D1%82%D1%8C-%D0%BA%D1%80%D1%83%D1%82%D1%8B%D0%B5-%D0%B0%D0%B2%D0%B0%D1%82%D0%B0%D1%80%D0%BA%D0%B8-%D0%B4%D0%BB%D1%8F-%D0%B2%D0%BA-%D0%B4%D0%BB%D1%8F-%D0%BF%D0%B0%D1%86%D0%B0%D0%BD%D0%BE%D0%B2019.jpg"},
    friends : [{type: mongoose.Types.ObjectId, required: false, default:[], ref: 'User', autopopulate:{ maxDepth: 3 }}],
    invitations : [{type: mongoose.Types.ObjectId, required: false, default:[], ref: 'User', autopopulate:{ maxDepth: 1 }}],
    posts:[{type: mongoose.Types.ObjectId, required: false, default:[], ref: 'Post', autopopulate:{maxDepth: 1}}]
  }) 
   
  UserSchema.plugin(require('mongoose-autopopulate'));
  const User = mongoose.model<UserInterface>("User", UserSchema);
  export {User,UserInterface};
 
 