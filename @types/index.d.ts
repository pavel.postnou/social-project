import {User,UserInterface} from "../models/userModel";

declare global{
    namespace Express {
        interface Request {
            user: UserInterface;
        }
    }
}
