import * as React from 'react';
import { View, StyleSheet } from 'react-native';
import BouncingPreloader from 'react-native-bouncing-preloaders';



export default function Loader () {
 return(
     <View style={styles.loader}>
<BouncingPreloader
  icons={[
    "https://img.icons8.com/color/480/000000/instagram-new--v1.png",
    "https://img.icons8.com/color/480/000000/twitter-circled--v4.png",
    'https://img.icons8.com/color/50/000000/facebook.png',
    "https://img.icons8.com/fluency/240/000000/vk-circled.png",
    "https://img.icons8.com/color/50/000000/viber.png",
    "https://img.icons8.com/fluency/240/000000/telegram-app.png"
  ]}
  leftRotation="-680deg"
  rightRotation="360deg"
  leftDistance={-180}
  rightDistance={-250}
  speed={1200} />
  </View>
 )
}

const styles = StyleSheet.create({

    loader: {
        justifyContent:"center",
        paddingTop:200
    }
})