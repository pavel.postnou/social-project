import React, {useState, useContext} from "react"
import { View, Text, StyleSheet } from "react-native"
import { Avatar, Icon } from 'react-native-elements'
import httpService from "../services/http.service"
import Context from '../context/context';

export default function InvitationItem({invitation}:any) {
    const[invit, setInvit] = useState(invitation)
    const context = useContext(Context)
    const myId = context.userState._id

    async function accept(id: string) {
        await httpService.addFriend(myId, id);
        const res = await httpService.getAllFriendsById(myId);
        setInvit(null)
        const invLength = res.user.invitations.length;
        context.handleSetData({invLength})
      }
    
      async function decline(id: string) {
        await httpService.declineFriend(myId, id);
        const res = await httpService.getAllFriendsById(myId);
        setInvit(null)
        const invLength = res.user.invitations.length;
        context.handleSetData({invLength})
      }

    return (
        invit != null ?
    <View style={styles.container}>
        <View>
            <View style={styles.container}>
                <Avatar size="small" rounded source={{ uri: invit?.avatar }} />
                <Text style={styles.title}> {invit?.firstName} {invit?.lastName} </Text>
            </View>
            <Text style={styles.title}>хочет быть вашим другом</Text>
        </View>
        <View>
            <Icon size={15} raised name='user-plus' type='font-awesome' color='darkblue' onPress={() => accept(invit._id)} />
        </View>
        <View>
            <Icon size={15} raised name='minus-circle' type='font-awesome' color='red' onPress={() => decline(invit._id)} />
        </View>
    </View>
    :
    null
    )
}

const styles = StyleSheet.create({

    block: {
      flex: 1
    },
    mainTitle: {
      fontSize: 20,
      fontWeight: "bold",
      color: "blue",
      marginLeft: 10
    },
    icon:{
      marginLeft:10
    },
    title: {
      color: "black",
      fontSize: 15,
      fontWeight: "bold",
      marginLeft:10
    },
    container: {
      marginTop: 10,
      marginLeft: 10,
      flexDirection: "row",
      justifyContent: "flex-start",
      backgroundColor:'rgba(222, 222, 222, 0.5)'
    },
    usersView: {
      alignSelf: "center",
      width: "100%",
      height: "100%",
      backgroundColor: 'rgba(222, 222, 222,0.5)',
      borderRadius: 10
    },
    image: {
      width: "100%",
      flex: 1,
    },
  });