import React, { useState, useContext } from "react"
import { Modal, View, Text, StyleSheet, Image, ScrollView, TextInput, TouchableHighlight } from "react-native"
import { Icon } from 'react-native-elements'
import httpService from "../services/http.service"
import Context from '../context/context';
import { Video } from "expo-av"
import * as ImagePicker from 'expo-image-picker';

export default function ModalPost({ comm, setModalVisible, setPosts }: any) {
    const context = useContext(Context)
    const id = context.userState._id;
    const [picture, setPicture] = useState<any>(null);
    const [video, setVideo] = useState<any>(null);
    const [title, setTitle] = useState('');
    const [text, setText] = useState('');

    async function addPost() {
        let date  = new Date()
        await httpService.addNewPost({ title, text, picture, video, owner: id, date });
        setPicture(null)
        setVideo(null)
        const res = await httpService.getAllUserPostsById(id);
        setPosts(res)
      }
      const takeImage = async () => {
        let result = await ImagePicker.launchCameraAsync({
          mediaTypes: ImagePicker.MediaTypeOptions.All,
          allowsEditing: true,
          aspect: [16, 9],
          quality: 1,
        });
        if (!result.cancelled) {
          setPicture(result.uri);
        }
      };
    
      const takeVideo = async () => {
        let result = await ImagePicker.launchCameraAsync({
          mediaTypes: ImagePicker.MediaTypeOptions.Videos,
          allowsEditing: true,
          aspect: [9, 19],
          quality: 1,
        });
        if (!result.cancelled) {
          setVideo(result.uri);
        }
      };

      const pickImage = async () => {
        let result = await ImagePicker.launchImageLibraryAsync({
          mediaTypes: ImagePicker.MediaTypeOptions.All,
          allowsEditing: true,
          aspect: [16, 9],
          quality: 1,
        });
        if (!result.cancelled) {
          setPicture(result.uri);
        }
      };

    return (
        <Modal
            animationType="fade"
            transparent={true}
            visible={true}>
            <View style={styles.centeredView}>
                <View style={styles.modalView}>
                    <ScrollView keyboardShouldPersistTaps='always'>
                        <TextInput style={styles.textInput} placeholder="title" onChangeText={text => setTitle(text)} value={title} ></TextInput>
                        <TextInput style={styles.textInput} placeholder="text" onChangeText={text => setText(text)} value={text} ></TextInput>
                        {picture ? <Image source={{ uri: picture }} style={{ width: 350, height: 200 }} /> : null}
                        {video ? <Video source={{ uri: video }} style={{ width: 350, height: 200 }} /> : null}
                        <View style={styles.picture}>
                            <Icon raised name='file-image-o' type='font-awesome' color='red' onPress={() => pickImage()} />
                            <Icon raised name='camera' type='font-awesome' color='black' onPress={() => takeImage()} />
                            <Icon raised name='video-camera' type='font-awesome' color='darkblue' onPress={() => takeVideo()} />
                        </View>
                        <TouchableHighlight
                            style={{ ...styles.modalTouch, backgroundColor: '#2196F3' }}
                            onPress={() => {
                                addPost(), setModalVisible(false);
                            }}>
                            <Text style={styles.text}>Добавить</Text>
                        </TouchableHighlight>
                        <TouchableHighlight
                            style={{ ...styles.modalTouch, backgroundColor: '#2196F3' }}
                            onPress={() => {
                                setModalVisible(!false), setPicture(null), setVideo(null);
                            }}>
                            <Text style={styles.text}>Отмена</Text>
                        </TouchableHighlight>
                    </ScrollView>
                </View>
            </View>
        </Modal>
    )
}

const styles = StyleSheet.create({

    image: {
        width: "100%",
        flex: 1,
    },
    icon: {
        position: "absolute",
        right: 20,
        bottom: 20
    },
    title: {
        color: "darkblue",
        fontSize: 20,
        fontFamily: "Staat"
    },
    close: {
        position: "absolute",
        top: 10,
        right: 15
    },
    date: {
        color: "blue",
        fontSize: 15,
        fontWeight: "bold",
        alignSelf: "flex-end"
    },
    raw: {
        flexDirection: "row",
    },
    commentsBlock: {
        flex: 1,
        height: 180
    },
    postBlock: {
        flex: 1,
        alignItems: 'center',
        backgroundColor: 'rgba(222, 222, 222, 0.5)',
        marginBottom: 10
    },
    scrollView: {
        backgroundColor: 'rgba(222, 222, 222, 0.5)',
    },
    textInput: {
        color: "black",
        height: 30,
        width: "100%",
        borderColor: '#adbd00',
        borderWidth: 2,
        paddingLeft: 10,
        backgroundColor: "#e6e6e6",
        borderRadius: 30,
        marginVertical: 20
    },
    textInputCom: {
        color: "black",
        height: 30,
        borderColor: '#adbd00',
        width: "90%",
        borderWidth: 2,
        marginTop: 10,
        paddingLeft: 10,
        backgroundColor: "#e6e6e6",
        borderRadius: 30
    },
    postTitle: {
        width: "100%",
        paddingLeft: 10,
        marginBottom: 10
    },
    commentsRow: {
        flexDirection: "row",
        width: "100%",
        paddingLeft: 10,
        height: 30,
        marginTop: 10
    },
    about: {
        color: "#1a1a1a",
        fontSize: 15,
        fontFamily: 'Staat'
    },
    logout: {
        marginLeft: 300,
        backgroundColor: "red",
        padding: 3,
        borderRadius: 30,
        width: "25%",
        alignItems: "center"
    },
    ava: {
        flex: 1
    },
    picture: {
        flexDirection: "row",
        justifyContent: "space-evenly"
    },
    infoView: {
        flex: 1,
        backgroundColor: 'rgba(222, 222, 222, 0.5)',
        padding: 10,
        flexDirection: "row"
    },
    centeredView: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 10,
    },

    modalView: {
        width: "90%",
        height: "70%",
        margin: 10,
        backgroundColor: 'white',
        borderRadius: 10,
        padding: 10,
        alignItems: "stretch",
        shadowColor: '#000',
        justifyContent: "space-evenly"
    },
    modalCom: {
        margin: 10,
        backgroundColor: 'white',
        borderRadius: 10,
        padding: 10,
        alignItems: "stretch",
        shadowColor: '#000',
        justifyContent: "space-evenly"
    },

    postView: {
        flex: 6,
        marginTop: 10
    },
    imgPost: {
        flex: 1,
        width: "100%",
        height: 235,
        resizeMode: "contain"
    },
    videoPost: {
        flex: 1,
        width: "100%",
        height: 500,
        resizeMode: "contain"
    },
    text: {
        color: "#1a1a1a",
        fontSize: 20,
        fontFamily: 'Staat'
    },
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    comContainer: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        marginBottom: 10,
        backgroundColor: "rgba(255, 255, 255, 0.5)",
        borderWidth: 2,
        borderColor: "#adbd00"

    },
    info: {
        flex: 2,
        alignItems: "flex-start",
        justifyContent: "space-evenly",
        paddingLeft: 20
    },
    modalTouch: {
        alignSelf: "center",
        backgroundColor: "#308aff",
        padding: 5,
        borderRadius: 30,
        width: "95%",
        marginBottom: 10,
        alignItems: "center",
    },
    likes: {
        color: "#ff0000",
        fontSize: 20,
        fontWeight: "bold",
        marginHorizontal: 10
    }
});
