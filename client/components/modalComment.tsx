import React, { useState, useContext } from "react"
import { Modal, View, Text, StyleSheet, Image, ScrollView, TextInput } from "react-native"
import { Avatar, Icon } from 'react-native-elements'
import httpService from "../services/http.service"
import Context from '../context/context';
import { Video } from "expo-av"
import CommentItem from "./commentItem";

export default function ModalComment({ comm, setModalVisible, setPost }: any) {
    const context = useContext(Context)
    const [post, setPostNew] = useState(comm)
    const [comment, setComment] = useState('');
    const id = context.userState._id;

    async function addComments(text: string, postId: string) {
        let date = new Date()
        await httpService.addNewComment(id, context.userState.firstName, context.userState.lastName, context.userState.avatar, text, postId, date);
        const postNew = await httpService.getCommentsByPostId(postId);
        setPost(postNew.post)
        setPostNew(postNew.post)
    }

    return (
        <Modal
            animationType="fade"
            transparent={true}
            visible={true}>
            <View style={styles.centeredView}>
                <ScrollView keyboardShouldPersistTaps='always'>
                    {post ?
                        <View style={styles.modalCom}>
                            <View style={styles.postTitle}>
                                <Avatar size="small" rounded source={{ uri: post.owner.avatar }} />
                                <Text style={styles.title}> {post.owner.firstName} {post.owner.lastName} </Text>
                            </View>
                            <View>
                                <Text>{new Date(post.date).toUTCString().slice(0, -3)}</Text>
                            </View>
                            <Text style={styles.title}>{post.title}</Text>
                            <Text style={styles.about}>{post.text}</Text>
                            {post.picture ? <Image resizeMode="contain" style={styles.imgPostModal} source={{ uri: post.picture }} /> : null}
                            {post.video ? <Video style={styles.imgPost} source={{ uri: post.video }} /> : null}
                            <View style={styles.commentsRow}>
                                <Icon name='heartbeat' type='font-awesome' color='#ff0000' />
                                <Text style={styles.likes}>{post.likes.length}</Text>
                                <Icon name='comment' type='font-awesome' color='#ff0000' />
                                <Text style={styles.likes}>{post.comments.length}</Text>
                            </View>
                            <View style={styles.commentsBlock}>
                                <ScrollView keyboardShouldPersistTaps='always' style={styles.scrollView}>
                                    {post.comments.length > 0 ? post.comments.map((line: any) => (<CommentItem data={line} key={line._id} setPost={setPost} setPostNew={setPostNew} />)) : null}
                                </ScrollView>
                            </View>
                            <View style={styles.close}>
                                <Icon name='times' type='font-awesome' color='black' onPress={() => { setModalVisible(false) }} />
                            </View>
                            <View style={styles.raw}>
                                <TextInput style={styles.textInputCom} placeholder="add comment" onChangeText={text => setComment(text)} value={comment} />
                                <Icon name='textsms' type='material-icons' color='black' onPress={() => { addComments(comment, post._id), setComment("") }} />
                            </View>
                        </View>
                        : null}
                </ScrollView>
            </View>
        </Modal>
    )
}

const styles = StyleSheet.create({

    postTitle: {
        width: "100%",
        paddingLeft: 10,
        paddingTop: 10,
        marginBottom: 10,
        flexDirection: "row"
    },
    title: {
        color: "black",
        fontSize: 20,
        fontWeight: "bold"
    },
    commentsRow: {
        flexDirection: "row",
        width: "100%",
        paddingLeft: 10,
        height: 30,
        marginTop: 10
    },
    about: {
        color: "#1a1a1a",
        fontSize: 15
    },
    centeredView: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 10,
    },
    imgPost: {
        flex: 1,
        width: "100%",
        height: 240,
        resizeMode: "contain"
    },
    imgPostModal: {
        flex: 1,
        width: "100%",
        height: 200,
        resizeMode: "contain"
    },
    likes: {
        color: "#ff0000",
        fontSize: 20,
        fontWeight: "bold",
        marginHorizontal: 10
    },
    close: {
        position: "absolute",
        top: 0,
        right: 15
    },
    raw: {
        flexDirection: "row",
    },
    commentsBlock: {
        flex: 1,
        height: 180
    },

    scrollView: {
        backgroundColor: 'rgba(222, 222, 222, 0.5)',
    },

    textInputCom: {
        color: "black",
        height: 30,
        borderColor: '#adbd00',
        width: "90%",
        borderWidth: 2,
        marginTop: 10,
        paddingLeft: 10,
        backgroundColor: "#e6e6e6",
        borderRadius: 30
    },
    modalCom: {
        margin: 10,
        backgroundColor: 'white',
        borderRadius: 10,
        padding: 10,
        alignItems: "stretch",
        shadowColor: '#000',
        justifyContent: "space-evenly"
    },
});