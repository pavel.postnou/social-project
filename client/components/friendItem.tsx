import React, {useState, useContext} from "react"
import { View, Text, StyleSheet, Alert } from "react-native"
import { Avatar, Icon } from 'react-native-elements'
import httpService from "../services/http.service"
import Context from '../context/context';
import { useNavigation } from '@react-navigation/native';

export default function FriendItem({item}:any) {
    const navigation: any = useNavigation();
    const[friend, setFriend] = useState(item)
    const context = useContext(Context)
    const myId = context.userState._id

    const deleteFriendAlert = (friend: any) => {
        Alert.alert(
          "Внимание",
          `Вы действительно хотите удалить ${friend.firstName} ${friend.lastName} из друзей?`,
          [
            {
              text: "OK", onPress: async () => {
                deleteFriend(friend._id)
              }
            },
            {
              text: "Cancel",
              style: "cancel"
            },
          ],
          { cancelable: false }
        );
      }

      async function deleteFriend(id: string) {
        await httpService.removeFriend(myId, id);
        setFriend(null)
      }
    
      async function chatWithFriend(friendId: string) {
        context.handleSetData({friendId})
        navigation.navigate('Chats')
      } 
 
    return ( 
        friend != null ?
        <View style={styles.container}>
        <Avatar size="small" rounded source={{ uri: friend.avatar }} />
        <Text style={styles.title}> {friend.firstName} {friend.lastName} </Text>
        <View style={styles.icon}>
        <Icon size={20} name='minus-circle' type='font-awesome' color='red' onPress={() => deleteFriendAlert(friend)} />
        </View>
        <View style={styles.icon}>
        <Icon size={20} name='comments' type='font-awesome' color='#5c5c5c' onPress={() => chatWithFriend(friend._id)}/>
        </View>
      </View>
    :
    null
    )
}

const styles = StyleSheet.create({

    icon:{
      marginLeft:10
    },
    title: {
      color: "black",
      fontSize: 15,
      fontWeight: "bold",
      marginLeft:10
    },
    container: {
      marginTop: 10,
      marginLeft: 10,
      flexDirection: "row",
      justifyContent: "flex-start",
      backgroundColor:'rgba(222, 222, 222, 0.5)'
    }
  });