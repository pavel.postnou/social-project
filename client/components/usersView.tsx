import React, { useContext,useState } from 'react';
import { Alert, StyleSheet,Text, View} from 'react-native';
import Context from '../context/context';
import { Avatar, Icon } from 'react-native-elements'
import httpService from '../services/http.service';

export default function Users({ user }: any) {
    const context = useContext(Context)
    const myId = context.userState._id
    const [item, setItem] = useState<any>(user)

    const askFriendAlert = (friend: any) => {
        Alert.alert(
            "Внимание",
            `Вы действительно хотите добавить ${friend.firstName} ${friend.lastName} в друзья?`,
            [
                {
                    text: "OK", onPress: async () => {
                        request(friend._id)
                    }
                },
                {
                    text: "Cancel",
                    style: "cancel"
                },
            ],
            { cancelable: false }
        );
    }

    async function request(id: string) {
        await httpService.askFriend(myId, id)
        const res = await httpService.getAllFriendsById(id);
        setItem(res.user)
    }

    return (
        <View >
            {item!=undefined?
            <View style={styles.user}>
            <Avatar size="small" rounded source={{ uri: item.avatar }} />
            <Text style={styles.title}> {item.firstName} </Text>
            <Text style={styles.title}>{item.lastName}</Text>
            {(item.friends.
                filter((friend: any) => (friend._id).
                    includes(myId)).length) > 0 ?
                <Text style={styles.description}> (Ваш друг)</Text>
                :
                (item._id === myId) ?
                    <Text style={styles.description}> (Вы)</Text>
                    :
                    (item.invitations.
                        filter((invitation: any) => (invitation._id).
                            includes(myId)).length) > 0 ?
                        <Text style={styles.description}> (Запрос в друзья отправлен)</Text>
                        :
                        <Icon size={15} raised name='user-plus' type='font-awesome' color='darkblue' onPress={() => askFriendAlert(user)} />
            }
            </View>:null}
        </View>
    )
}

const styles = StyleSheet.create({

    title: {
        color: "black",
        fontSize: 15,
        fontWeight: "bold"
    },
    description: {
        color: "red",
        fontSize: 12,
        fontWeight: "bold"
    },
    user: {
        flex: 1,
        flexDirection: "row",
        paddingTop: 3,
        paddingLeft: 10,
        marginBottom: 5,
        height: 50,
        backgroundColor: 'rgba(222, 222, 222,0.5)'
    }
});