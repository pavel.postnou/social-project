import React, { useState, useContext } from "react"
import {View,Alert, Text, StyleSheet} from "react-native"
import { Avatar, Icon } from 'react-native-elements'
import httpService from "../services/http.service"
import Context from '../context/context';

export default function CommentItem({ data, setPost, setPostNew }: any) {
    const [line, setLine] = useState(data)
    const context = useContext(Context)
    const id = context.userState._id;

    const deleteComAlert = (comId:string, postId:string) => {
        Alert.alert(
          "Внимание",
          `Вы действительно хотите удалить комментарий?`,
          [
            {
              text: "OK", onPress: async () => {
                deleteComment(comId, postId)
              }
            },
            {
              text: "Cancel",
              style: "cancel"
            },
          ],
          { cancelable: false }
        );
      }

    async function deleteComment(commentId: string, postId: string) {
        await httpService.deleteComment(commentId);
        const post = await httpService.getCommentsByPostId(postId);
        setPost(post.post)
        setPostNew(post.post)
        setLine(null)
    }

    return (
        line != null ?
            <View style={styles.comContainer}>
                <View>
                    <Text>{new Date(line.date).toUTCString().slice(0, -3)}</Text>
                </View>
                <Avatar size="small" rounded source={{ uri: line.avatar }} />
                <Text>{line.firstName} {line.lastName}</Text>
                <Text>{line.text}</Text>
                {
                    line.authorId === id ? <View style={styles.close}>
                        <Icon name='trash' type='font-awesome' color='black' onPress={() => { deleteComAlert(line._id, line.postId) }} />
                    </View> : null
                }
            </View>
            : null
    )
}

const styles = StyleSheet.create({
    close: {
        position: "absolute",
        top: 0,
        right: 15
    },
    comContainer: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        marginBottom: 10,
        backgroundColor: "rgba(255, 255, 255, 0.5)",
        borderWidth: 2,
        borderColor: "#adbd00"

    },

});