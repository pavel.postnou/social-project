import React, {useState, useContext} from "react"
import { View, Text, StyleSheet } from "react-native"
import { Avatar, Icon } from 'react-native-elements'
import httpService from "../services/http.service"
import Context from '../context/context';

export default function ChatItem({item}:any) {
const [message, setMessage] = useState(item)
const context = useContext(Context)
const myId = context.userState._id;
return (
    message.author._id === myId ?
        <View style={styles.myContainer}>
          <View style={styles.myContainerOne}>
          <View style={styles.message}>
            <Text style={styles.text}> {message.text} </Text>
          </View>
          <Avatar size="small" rounded source={{ uri: message.author.avatar }} />
          </View>
        </View>
        :
        <View style={styles.friendContainer}>
          <Avatar size="small" rounded source={{ uri: message.author.avatar }} />
          <View style={styles.message}>
            <Text style={styles.text}> {message.text} </Text>
          </View>
        </View>
)
}

const styles = StyleSheet.create({

    message: {
      maxWidth:"75%",
      color: "black",
      borderColor: 'grey',
      width: "auto",
      borderWidth: 2,
      marginHorizontal: 5,
      paddingHorizontal: 5,
      backgroundColor: "#e6e6e6",
      borderRadius: 10
    },
    text: {
      color: "#1a1a1a",
      fontSize: 15,
      borderColor: "black"
    },
    friendContainer: {
      maxWidth:"45%",
      marginLeft: 10,
      marginTop: 10,
      alignItems: "flex-start",
      flexDirection:"row"
    },
    myContainer: {
      marginRight: 10,
      marginTop: 10,
      alignItems: "flex-end",
    },
    myContainerOne: {
      marginRight: 10,
      marginTop: 10,
      alignItems: "flex-end",
      flexDirection:"row"
    },
  });
  