import React, { useState, useContext } from "react"
import { View, Text, StyleSheet, Image, Alert } from "react-native"
import { Avatar, Icon } from 'react-native-elements'
import httpService from "../services/http.service"
import Context from '../context/context';
import { Video } from "expo-av"
import ModalComment from "./modalComment";

export default function PostItem({ item }: any) {
    const [post, setPost] = useState(item)
    const [modalComVisible, setModalComVisible] = useState(false);
    const context = useContext(Context)
    const id = context.userState._id;

    const deletePostAlert = (postId: string) => {
        Alert.alert(
            "Внимание",
            `Вы действительно хотите удалить пост?`, 
            [
                {
                    text: "OK", onPress: async () => {
                        deletePost(postId)
                    }
                },
                {
                    text: "Cancel",
                    style: "cancel"
                },
            ],
            { cancelable: false }
        );
    }

    async function deletePost(postId: string) {
        await httpService.deletePost(postId);
        setPost(null)
    }

    async function like(postId: string) {
        const postLike: any = await httpService.likePostById(postId, id);
        setPost(postLike)
    }

    return (
        post != null?
        <View>
            <View style={styles.postTitle}>
                <Avatar size="small" rounded source={{ uri: post.owner.avatar }} />
                <Text style={styles.title}> {post.owner.firstName} {post.owner.lastName} </Text>
            </View>
            <View style={styles.postText}>
                <Text>{new Date(post.date).toUTCString().slice(0, -3)}</Text>
                <Text style={styles.title}>{post.title}</Text>
                <Text style={styles.about}>{post.text}</Text>
            </View>
            {post.picture ? <Image resizeMode="contain" style={styles.imgPost} source={{ uri: post.picture }} /> : null}
            {post.video ? <Video style={styles.imgPost} source={{ uri: post.video }} /> : null}
            <View style={styles.commentsRow}>
                <Icon name='heartbeat' type='font-awesome' color='#ff0000' onPress={() => like(post._id)} />
                <Text style={styles.likes}>{post.likes.length}</Text>
                <Icon name='comment' type='font-awesome' color='#ff0000' onPress={() => { setModalComVisible(!modalComVisible) }} />
                <Text style={styles.likes}>{post.comments.length}</Text>
            </View>
            {modalComVisible ? <ModalComment comm={post} setModalVisible={setModalComVisible} setPost={setPost} /> : null}
            {post.owner._id === id ?
                <View style={styles.close}>
                    <Icon name='trash' type='font-awesome' color='black' onPress={() => { deletePostAlert(item._id) }} />
                </View>
                :
                null}
        </View>
        :
        null
    )
}

const styles = StyleSheet.create({

    mainTitle: {
        fontSize: 20,
        fontWeight: "bold",
        color: "blue",
        marginLeft: 10
    },
    postTitle: {
        width: "100%",
        paddingLeft: 10,
        paddingTop: 10,
        marginBottom: 10,
        flexDirection: "row"
    },
    postText: {
        width: "100%",
        paddingLeft: 10,
        marginBottom: 10
    },
    title: {
        color: "black",
        fontSize: 20,
        fontWeight: "bold"
    },
    commentsRow: {
        flexDirection: "row",
        width: "100%",
        paddingLeft: 10,
        height: 30,
        marginTop: 10
    },
    about: {
        color: "#1a1a1a",
        fontSize: 15
    },
    imgPost: {
        flex: 1,
        width: "100%",
        height: 240,
        resizeMode: "contain"
    },
    likes: {
        color: "#ff0000",
        fontSize: 20,
        fontWeight: "bold",
        marginHorizontal: 10
    },
    close: {
        position: "absolute",
        top: 0,
        right: 15
    },
});