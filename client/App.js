
import React, {useReducer} from 'react';
import  {NavigationContainer}  from '@react-navigation/native';
import Context from "./context/context";
import * as ACTIONS from "./actions/users.action";
import { ReducerFunction, defaultState } from "./context/reducer";
import { MainStackNavigator } from "./navigation/mainNavigation";
import { StripeProvider } from '@stripe/stripe-react-native';

export default function App() {
  const publishableKey = "pk_test_51JwmByBCGSa6irvE39b37uhBBkExVnqVeYm9Ouq40RwTDpLP5IrTbgNi4PwB4oQOETCEYOVHhbyRQvBGHZSxZ0iJ00ziBSEmph";
  const [stateUser, dispatchUserReducer] = useReducer(ReducerFunction, defaultState);
  const handleSetData = (data) => {
    dispatchUserReducer(ACTIONS.setUser(data));
  };
 
  return (
    <StripeProvider
    publishableKey={publishableKey}
    urlScheme="your-url-scheme"
  >
   <Context.Provider value={{
      userState: stateUser,
      handleSetData: (data) => handleSetData(data)
    }}>
      <NavigationContainer>
      <MainStackNavigator/>
      </NavigationContainer>
    </Context.Provider>
    </StripeProvider>
  );
}

