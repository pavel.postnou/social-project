import { useFonts } from 'expo-font';
import React from 'react';
export default function getFonts () {
const [fontsLoaded] = useFonts({
    'Dancing': require('../assets/fonts/DancingScript-Bold.ttf'),
    'Rampart': require('../assets/fonts/RampartOne-Regular.ttf'),
    'Permanent': require('../assets/fonts/PermanentMarker-Regular.ttf'),
  });
  if (!fontsLoaded) {
    return null;
  }
}
