import { Alert } from 'react-native';
import { getTokenInfo, storeTokenInfo } from './asyncStorage.service';
import { io } from "socket.io-client";
import { loginSchema } from "../utils/login";
import { registrationSchema } from "../utils/registration";

interface iData {
    firstName?: string,
    lastName?: string,
    email: string,
    password: string

};

interface iNewPost {
    title: string,
    owner: string,
    text: string,
    picture?: string,
    video?: string,
    date: Date
}
const URL = 'http://192.168.1.46:3000';
export const socket = io(URL, { autoConnect: false });

class Http {

    signUp = async (data: iData): Promise<any> => {
        try {
            await registrationSchema.validate(data)
            const res = await fetch(`${URL}/register`, {
                method: 'POST',
                body: JSON.stringify(data),
                headers: {
                    'Content-Type': 'application/json'
                }
            });
            const json = await res.json();
            if (res.status === 200) {
                return json
            }
            else if (res.status === 400) {
                Alert.alert("Error", json)
            }
            else if (res.status === 409) {
                Alert.alert("Error", json)
            }
            else {
                Alert.alert('Error', 'Something went wrong... Server is not answering!!!');
                return false;
            }
        }
        catch (e: any) {
            Alert.alert("Error", `${e.message}`)
        }
    }

    logIn = async (data: iData): Promise<any> => {
        try {
            await loginSchema.validate(data)
            const res = await fetch(`${URL}/login`, {
                method: 'POST',
                body: JSON.stringify(data),
                headers: {
                    'Content-Type': 'application/json'
                }
            });
            const json = await res.json();
            if (res.status === 200) {
                storeTokenInfo({
                    accessToken: json.accessToken,
                });
                return json.user
            }
            else if (res.status === 402) {
                Alert.alert("Error", json)
                return false
            }
            else if (res.status === 401) {
                Alert.alert("Error", json)
                return false
            }
            Alert.alert("Error", json)
        }
        catch (e: any) {
            Alert.alert("Error", `${e.message}`)
            return false;
        }
    }

    getAllUserPostsById = async (id: string): Promise<any> => {
        try {
            const tokenInfo = getTokenInfo && await getTokenInfo();
            if (tokenInfo && typeof tokenInfo !== 'boolean') {
                const res = await fetch(`${URL}/post/${id}`, {
                    method: "GET",
                    headers: {
                        'Content-Type': 'application/json',
                        'Authorization': `Bearer ${tokenInfo.accessToken}`
                    }
                });
                const json = await res.json();
                if (res.status === 200) {
                    return json
                }
                Alert.alert("Error", json)
                return false
            }
        } catch (e:any) {
            Alert.alert("Error", `${e.message}`)
            return false;
        }
    }

    addNewPost = async (post: iNewPost): Promise<boolean> => {
        try {
            const tokenInfo = getTokenInfo && await getTokenInfo();

            if (tokenInfo && typeof tokenInfo !== 'boolean') {

                const res = await fetch(`${URL}/post/create`, {
                    method: 'POST',
                    body: JSON.stringify(post),
                    headers: {
                        'Content-Type': 'application/json',
                        'Authorization': `Bearer ${tokenInfo.accessToken}`
                    }
                });
                const json = await res.json();
                if (res.status === 200) {
                    return true
                }
                Alert.alert("Error", json)
                return false
            }
            Alert.alert('Error', 'You can not create new post. You have no authorization!!!')
            return false;
        } catch (e:any) {
            Alert.alert("Error", `${e.message}`)
            return false;
        }
    }

    deletePost = async (id: any): Promise<boolean> => {
        try {
            const tokenInfo = getTokenInfo && await getTokenInfo();

            if (tokenInfo && typeof tokenInfo !== 'boolean') {

                const res = await fetch(`${URL}/post/delete/${id}`, {
                    method: 'PUT',
                    headers: {
                        'Content-Type': 'application/json',
                        'Authorization': `Bearer ${tokenInfo.accessToken}`
                    }
                });

                const json = await res.json();
                if (res.status === 200) {
                    return true
                }
                Alert.alert("Error", json)
                return false
            }
            Alert.alert('Error', 'You can not create new post. You have no authorization!!!')
            return false;
        } catch (e:any) {
            Alert.alert("Error", `${e.message}`)
            return false;
        }
    }

    likePostById = async (postId: string, userId: string): Promise<boolean> => {
        try {
            const tokenInfo = getTokenInfo && await getTokenInfo();
            if (tokenInfo && typeof tokenInfo !== 'boolean') {
                const res = await fetch(`${URL}/post/like`, {
                    method: 'PUT',
                    headers: {
                        'Content-Type': 'application/json',
                        'Authorization': `Bearer ${tokenInfo.accessToken}`
                    },
                    body: JSON.stringify({ postId, userId })
                });
                const json = await res.json();
                if (res.status === 200) {
                    return json
                }
                Alert.alert("Error")
                return false
            } else {
                return false;
            }
        } catch (e:any) {
            Alert.alert("Error", `${e.message}`)
            return false;
        }
    }

    getAllUsers = async () => {
        try {
            const tokenInfo = getTokenInfo && await getTokenInfo();
            if (tokenInfo && typeof tokenInfo !== 'boolean') {
                const res = await fetch(`${URL}/users/showAll`, {
                    method: "GET",
                    headers: {
                        'Authorization': `Bearer ${tokenInfo.accessToken}`
                    }
                });
                const json = await res.json();
                if (json) {
                    return json.users;
                }
                Alert.alert("Error", json)
                return false;
            }
        } catch (e:any) {
            Alert.alert("Error", `${e.message}`)
            return false;
        }
    }

    getAllFriendsById = async (id: string) => {
        try {
            const tokenInfo = getTokenInfo && await getTokenInfo();
            if (tokenInfo && typeof tokenInfo !== 'boolean') {
                const res = await fetch(`${URL}/users/friends/${id}`, {
                    method: "GET",
                    headers: {
                        'Authorization': `Bearer ${tokenInfo.accessToken}`
                    }
                });
                const json = await res.json();
                if(res.status === 200) {
                    return json;
                }
                Alert.alert("Error", json)
                return false;
            }
        } catch (e:any) {
            Alert.alert("Error", `${e.message}`)
            return false;
        }
    }

    askFriend = async (myId: string, friendId: string) => {
        try {
            const tokenInfo = getTokenInfo && await getTokenInfo();
            if (tokenInfo && typeof tokenInfo !== 'boolean') {
                const res = await fetch(`${URL}/users/askFriend`, {
                    method: "PUT",
                    headers: {
                        'Content-Type': 'application/json',
                        'Authorization': `Bearer ${tokenInfo.accessToken}`
                    },
                    body: JSON.stringify({ myId, friendId })
                });
                const json = await res.json();
                if(res.status === 200) {
                    return json;
                }
                Alert.alert("Error", json)
                return false;
            }
        } catch (e:any) {
            Alert.alert("Error", `${e.message}`)
            return false;
        }
    }

    declineFriend = async (myId: string, friendId: string) => {

        try {
            const tokenInfo = getTokenInfo && await getTokenInfo();
            if (tokenInfo && typeof tokenInfo !== 'boolean') {
                const res = await fetch(`${URL}/users/del/declineFriend`, {
                    method: "PUT",
                    headers: {
                        'Content-Type': 'application/json',
                        'Authorization': `Bearer ${tokenInfo.accessToken}`
                    },
                    body: JSON.stringify({ myId, friendId })
                });
                const json = await res.json();
                if(res.status === 200) {
                    return json;
                }
                Alert.alert("Error", json)
                return false;
            }
        } catch (e:any) {
            Alert.alert("Error", `${e.message}`)
            return false;;
        }
    }

    addFriend = async (myId: string, friendId: string) => {
        try {
            const tokenInfo = getTokenInfo && await getTokenInfo();
            if (tokenInfo && typeof tokenInfo !== 'boolean') {
                const res = await fetch(`${URL}/users/friends/add`, {
                    method: "PUT",
                    headers: {
                        'Authorization': `Bearer ${tokenInfo.accessToken}`,
                        'Content-Type': 'application/json'
                    },
                    body: JSON.stringify({ myId, friendId })
                });
                
                const json = await res.json();
                if (res.status === 200) {
                    return true;
                }
                else {
                Alert.alert('Error', json)
                return false;
                }
            }
            return false
        } catch (e:any) {
            Alert.alert("Error", `${e.message}`)
            return false;
        }
    }

    removeFriend = async (myId: string, friendId: string) => {
        try {
            const tokenInfo = getTokenInfo && await getTokenInfo();
            if (tokenInfo && typeof tokenInfo !== 'boolean') {
                const res = await fetch(`${URL}/users/friends`, {
                    method: "PUT",
                    headers: {
                        'Authorization': `Bearer ${tokenInfo.accessToken}`,
                        'Content-Type': 'application/json'
                    },
                    body: JSON.stringify({ myId, friendId })
                });
                const json = await res.json();
                if (res.status === 200) {
                    return true;
                }
                else {
                Alert.alert("Error", json)
                return false;
                }
            }
            return false
        } catch (e:any) {
            Alert.alert("Error", `${e.message}`)
            return false;
        }
    }

    getNews = async (id: string) => {
        try {
            const tokenInfo = getTokenInfo && await getTokenInfo();
            if (tokenInfo && typeof tokenInfo !== 'boolean') {
                const res = await fetch(`${URL}/posts/news/${id}`, {
                    method: "GET",
                    headers: {
                        'Authorization': `Bearer ${tokenInfo.accessToken}`
                    }
                });
                const json = await res.json();
                return json;
            }
        } catch (e:any) {
            Alert.alert("Error", `${e.message}`)
            return false;
        }
    }

    getCommentsByPostId = async (postId: string) => {
        try {
            const tokenInfo = getTokenInfo && await getTokenInfo();
            if (tokenInfo && typeof tokenInfo !== 'boolean') {
                const res = await fetch(`${URL}/comments/${postId}`, {
                    method: "GET",
                    headers: {
                        'Authorization': `Bearer ${tokenInfo.accessToken}`
                    }
                });
                const json = await res.json();
                if (res.status === 200) {
                    return json;
                }
                Alert.alert('Error', json);
                return false;
            }
        } catch (e:any) {
            Alert.alert("Error", `${e.message}`)
            return false;
        }
    }

    addNewComment = async (authorId: string, firstName: string, lastName: string, avatar: string, text: string, postId: string, date: any) => {
        try {
            const tokenInfo = getTokenInfo && await getTokenInfo();
            if (tokenInfo && typeof tokenInfo !== 'boolean') {
                const res = await fetch(`${URL}/comments/add`, {
                    method: "POST",
                    headers: {
                        'Authorization': `Bearer ${tokenInfo.accessToken}`,
                        'Content-Type': 'application/json'
                    },
                    body: JSON.stringify({ authorId, firstName, lastName, avatar, text, postId, date })
                });
                const json = await res.json();
                if (res.status === 200) {
                    return true;
                }
                Alert.alert('Error', json);
                return false;
            }
            return false
        } catch (e:any) {
            Alert.alert("Error", `${e.message}`)
            return false;
        }
    }

    deleteComment = async (commentId: string) => {
        try {
            const tokenInfo = getTokenInfo && await getTokenInfo();
            if (tokenInfo && typeof tokenInfo !== 'boolean') {
                const res = await fetch(`${URL}/comments/delete/${commentId}`, {
                    method: "PUT",
                    headers: {
                        'Authorization': `Bearer ${tokenInfo.accessToken}`,
                        'Content-Type': 'application/json'
                    }
                });
                const json = await res.json();
                if (res.status === 200) {
                    return true;
                }
                Alert.alert('Error', json);
                return false;
            }
            return false
        } catch (e:any) {
            Alert.alert("Error", `${e.message}`)
            return false;
        }
    }

    changeAvatar = async (userId: string, newAvatar: string) => {
        try {
            const tokenInfo = getTokenInfo && await getTokenInfo();
            if (tokenInfo && typeof tokenInfo !== 'boolean') {
                const res = await fetch(`${URL}/profile/change`, {
                    method: "PUT",
                    headers: {
                        'Authorization': `Bearer ${tokenInfo.accessToken}`,
                        'Content-Type': 'application/json'
                    },
                    body: JSON.stringify({
                        userId,
                        newAvatar
                    })
                });
                const json = await res.json();
                if (res.status === 200) {
                    return true
                }
                Alert.alert("Error", json)
                return false
            }
            return false;
        } catch (e:any) {
            Alert.alert("Error", `${e.message}`)
            return false;
        }
    }

    chatWithFriend = async (myId: string, friendId: string) => {
        try {
            const tokenInfo = getTokenInfo && await getTokenInfo();
            if (tokenInfo && typeof tokenInfo !== 'boolean') {
                const res = await fetch(`${URL}/chats/`, {
                    method: "PUT",
                    headers: {
                        'Authorization': `Bearer ${tokenInfo.accessToken}`,
                        'Content-Type': 'application/json'
                    },
                    body: JSON.stringify({
                        myId,
                        friendId
                    })
                });
                const json = await res.json();
                if (res.status === 200) {
                    return json;
                }
                Alert.alert("Error",json)
            }
        } catch (e:any) {
            Alert.alert("Error", `${e.message}`)
            return false;
        }
    }

}

export default new Http();
