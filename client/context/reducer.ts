import {SET_USER} from './types'
export const defaultState = {
}

export const ReducerFunction =  (state = defaultState, action:any) => {
    switch (action.type) {
        case SET_USER:
            let newState = {
                ...state,...action.data
            }
            return newState;
        default:
            return state;
    }
};
