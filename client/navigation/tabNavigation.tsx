import React, { useEffect, useContext, useState } from "react";
import { Alert, TouchableHighlight, Text, StyleSheet } from 'react-native';
import MainScreen from '../screens/main';
import PostsScreen from '../screens/friendsPostsScreen'
import FriendsScreen from '../screens/friendsScreen'
import ChatsScreen from '../screens/chatsScreen'
import UsersScreen from '../screens/usersScreen'
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import Context from "../context/context";
import { Icon } from 'react-native-elements'
import { getFocusedRouteNameFromRoute } from '@react-navigation/native';

const Tab = createBottomTabNavigator();

const BottomTabNavigator = ({ navigation }: { navigation: any }) => {
  const context = useContext(Context)
  const [badge, setBadge] = useState<any>('');

  const logout = () => {
    Alert.alert(
      "Log Out",
      "Are You sure, you want to exit?",
      [
        {
          text: "OK", onPress: async () => {
            context.userState.friendId = null
            navigation.navigate('Login');
          }
        },
        {
          text: "Cancel",
          style: "cancel"
        },
      ],
      { cancelable: false }
    );
  }

  function getHeaderTitle(route: any) {
    const routeName = getFocusedRouteNameFromRoute(route) ?? 'Profile';
    switch (routeName) {
      case 'Profile':
        return context.userState.firstName + " " + context.userState.lastName;
    }
  }

  useEffect(() => {
    if (context.userState.invLength == 0) {
      setBadge(null)
    }
    else {
      setBadge(context.userState.invLength)
    }
  }, [context.userState.invLength])

  return (
    <Tab.Navigator
      screenOptions={({ route }) => ({
        tabBarIcon: ({ color, size }) => {
          let iconName: any;
          let iconType: any;
          if (route.name === 'Profile') {
            iconName = 'user';
            iconType = "font-awesome"
          } else if (route.name === 'News') {
            iconName = 'newspaper';
            iconType = "font-awesome-5"
          }
          else if (route.name === 'Friends') {
            iconName = 'users';
            iconType = "font-awesome"
          }
          else if (route.name === 'Find Friends') {
            iconName = 'user-plus';
            iconType = "font-awesome"
          }
          else if (route.name === 'Chats') {
            iconName = 'comments';
            iconType = "font-awesome"
          }
          return <Icon name={iconName} type={iconType} color='#f50' />;
        },
        tabBarActiveTintColor: 'red',
        tabBarInactiveTintColor: 'gray',
      })}>
      <Tab.Screen name="Profile" component={MainScreen} options={({ navigation, route, }) => ({
        headerRight: () => (
          <TouchableHighlight
            style={styles.logout} onPress={() => logout()}>
            <Text style={styles.touchText}>LogOut</Text>
          </TouchableHighlight>
        ),
        headerTitle: getHeaderTitle(route)
      })}
      />
      <Tab.Screen name="News" initialParams={{ param: true }} component={PostsScreen} options={({ navigation, route }) => ({
        headerRight: () => (
          <TouchableHighlight
            style={styles.logout} onPress={() => logout()}>
            <Text style={styles.touchText}>LogOut</Text>
          </TouchableHighlight>
        ),
        headerTitle: getHeaderTitle(route)
      })} />
      <Tab.Screen name="Friends" component={FriendsScreen} options={({ navigation, route }) => ({
        headerRight: () => (
          <TouchableHighlight
            style={styles.logout} onPress={() => logout()}>
            <Text style={styles.touchText}>LogOut</Text>
          </TouchableHighlight>
        ),
        tabBarBadge: badge,
        headerTitle: getHeaderTitle(route)
      })} />
      <Tab.Screen name="Chats" component={ChatsScreen} options={({ navigation, route }) => ({
        headerRight: () => (
          <TouchableHighlight
            style={styles.logout} onPress={() => logout()}>
            <Text style={styles.touchText}>LogOut</Text>
          </TouchableHighlight>
        ),
        headerTitle: getHeaderTitle(route)
      })} />
      <Tab.Screen name="Find Friends" initialParams={{ param: true }} component={UsersScreen} options={({ navigation, route }) => ({
        headerRight: () => (
          <TouchableHighlight
            style={styles.logout} onPress={() => logout()}>
            <Text style={styles.touchText}>LogOut</Text>
          </TouchableHighlight>
        ),
        headerTitle: getHeaderTitle(route)
      })} />
    </Tab.Navigator>
    
  );
};

export default BottomTabNavigator;

const styles = StyleSheet.create({

  logout: {
    marginRight: 10,
    backgroundColor: "red",
    padding: 3,
    borderRadius: 30,
    width: 100,
    alignItems: "center"
  },
  touchText: {
    color: "white",
    fontSize: 20,
    fontWeight: "bold"
  },
});
