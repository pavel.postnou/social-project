import React from "react";
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import HomeScreen from '../screens/login'
import RegScreen from "../screens/registration";
import BottomTabNavigator from "../navigation/tabNavigation"
import Card from "../screens/paymentScreen";
import ArtPay from "../screens/paymentScreenArt";

const Stack = createNativeStackNavigator();

const screenOptionStyle = {
  headerStyle: {
    backgroundColor: "white",
  },
  headerTintColor: "black",
  headerBackTitle: "Back",
};

const MainStackNavigator = () => {
  return (
    <Stack.Navigator initialRouteName="Login" screenOptions={screenOptionStyle}>
      <Stack.Screen name="Login" component={HomeScreen} options={{headerShown:false}}/>
      <Stack.Screen name="Registration" component={RegScreen} />
      <Stack.Screen name="Payment" component={Card} options={{ headerShown: false }}/>
      <Stack.Screen name="ArtPay" component={ArtPay} options={{ headerShown: false }}/>
      <Stack.Screen name="Main" component={BottomTabNavigator} options={{ headerShown: false }}/>
    </Stack.Navigator>
  )
}
export { MainStackNavigator };