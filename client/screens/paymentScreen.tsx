import React, { useState, useContext } from "react";
import { Alert, StyleSheet, TextInput, Text, Button, View } from "react-native";
import { useStripe } from "@stripe/stripe-react-native";
import Context from "../context/context";

export default function Card({ navigation }: { navigation: any }) {
  const { initPaymentSheet, presentPaymentSheet } = useStripe();
  const [amount, setAmount] = useState("");
  const API_URL = "http://192.168.1.46:3000";
  const context = useContext(Context);
  let email = context.userState.email;
  let active = false;

  const fetchPaymentSheetParams = async () => {
    const response = await fetch(`${API_URL}/payment/checkout`, {
      method: "POST",
      body: JSON.stringify({ amount, email }),
      headers: {
        "Content-Type": "application/json",
      },
    });
    const { paymentIntent, ephemeralKey, customer } = await response.json();

    return {
      paymentIntent,
      ephemeralKey,
      customer,
    };
  };

  const startSubscribing = async () => {
    const response = await fetch(`${API_URL}/payment/checkout/pay/subscribe`, {
      method: "POST",
      body: JSON.stringify({ amount, email }),
      headers: {
        "Content-Type": "application/json",
      },
    });
    let error = await response.json()
    
    response.status == 200 ? Alert.alert("Подписка успешно оформлена"): Alert.alert(error.error.message)

  };

  const initializePaymentSheet = async () => {
    const { paymentIntent, ephemeralKey, customer } =
      await fetchPaymentSheetParams();
    console.log(paymentIntent, ephemeralKey, customer);
    const { error } = await initPaymentSheet({
      customerId: customer,
      customerEphemeralKeySecret: ephemeralKey,
      paymentIntentClientSecret: paymentIntent,
    });
  };

  const openPaymentSheet = async () => {
    await initializePaymentSheet();
    const { error } = await presentPaymentSheet();
    if (error) {
      Alert.alert(`Error code: ${error.code}`, error.message);
    } else {
      Alert.alert("Success", "Your order is confirmed!");
      active = false;
      let loading = false
      await context.handleSetData({loading});
      navigation.navigate("Main");
    }
  };

  const correct = (text: string) => {
    setAmount(text.replace(/\./g, "").replace(/[^0-9]/g, "").replace(/(([0-9])([0-9])$)/g, ".$2$3"));
  };
  Number.parseFloat(amount) < 0.5||amount.length == 0 ? (active = true) : (active = false);

  return (
    <View style={styles.container}>
      <Text style={styles.text}>Для отключения рекламы требуется оплата</Text>
      <Text>Введите сумму сколько не жалко</Text>
      <Text>Минимум 0.5 usd</Text>
      <TextInput
        style={styles.textInputCom}
        keyboardType="numeric"
        placeholder="0.00"
        onChangeText={(text) => correct(text)}
        value={amount}
      />
      <View style={styles.button}> 
      <Button title="Checkout" onPress={openPaymentSheet} disabled={active} />
      </View>
      <Text style={styles.text}>Подписаться на ежемесячную оплату</Text>
      <View style={styles.button}>
      <Button title="Subscribe" onPress={startSubscribing} disabled={active} />
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
  },
  text:{
    marginTop:20,
    fontSize:17,
    color:"black"
  },
  textInputCom: {
    color: "black",
    height: 30,
    borderColor: "#adbd00",
    width: "30%",
    borderWidth: 2,
    marginVertical: 10,
    marginLeft: 10,
    paddingLeft: 10,
    backgroundColor: "#e6e6e6",
    borderRadius: 30,
  },
  button: {
    marginTop:10
  }
});
