import React, { useContext, useEffect, useState } from 'react';
import { StyleSheet,Text, View, ImageBackground, ScrollView} from 'react-native';
import Context from '../context/context';
import httpService from '../services/http.service';
import { img } from "../constants/backgroundImage"
import PostItem from '../components/postItem';

export default function MainScreen({route}:any) {

  const context = useContext(Context)
  const [friends, setFriends] = useState<any[]>([])
  const [name, setName] = useState("");
  const [surname, setSurname] = useState("");

  const id = context.userState._id;
  useEffect(() => {
    (async () => {
      setName(context.userState.firstName)
      setSurname(context.userState.lastName)
      const id = context.userState._id
      const res = await httpService.getAllFriendsById(id);
      setFriends(res.user.friends)
    })()
  }, [route.params])

  return (
    <View style={styles.block}>
      <ImageBackground source={img} resizeMode="cover" style={styles.image}>
        <Text style={styles.mainTitle}>Новости</Text>
        <ScrollView keyboardShouldPersistTaps='always'>
          {friends.length > 0 ? friends.map(item => (<View key={item._id}>
            {item.posts.length > 0 ? item.posts.map((post: any) => (<PostItem item={post} key={post._id}/>
            )) : null}
          </View>
          )) : null}
        </ScrollView>
      </ImageBackground>
    </View>
  )
}

const styles = StyleSheet.create({

  block: {
    flex: 1
  },
  mainTitle: {
    fontSize: 20,
    fontWeight: "bold",
    color: "blue",
    marginLeft: 10
  },
  image: {
    width: "100%",
    flex: 1,
  },
});