import { StyleSheet, View, ImageBackground, ScrollView, TextInput } from 'react-native';
import React, { useEffect, useState } from 'react';
import httpService from '../services/http.service';
import { img } from "../constants/backgroundImage"
import Loader from '../components/loader';
import Users from "../components/usersView"

export default function UsersScreen({ route }: any) {
    const [users, setUsers] = useState<any[]>()
    const [search, setSearch] = useState("")
    const [loading, setLoading] = useState(false)

    useEffect(() => {
        (async () => {
            setLoading(true)
            const res = await httpService.getAllUsers();
            res ? (setUsers(res), setLoading(false)) : null
        })()
    }, [route.params])

    return (
        <View style={styles.centeredView}>
            {loading ?
                <Loader />
                :
                <View style={styles.container}>
                    <ImageBackground source={img} resizeMode="cover" style={styles.image}>
                        <TextInput style={styles.textInput} placeholder="Type Here..." onChangeText={text => setSearch(text)} value={search} />
                        <ScrollView style={styles.usersScroll}>
                            {users && users.length > 0 ? users.
                                filter((user: any) => (user.firstName.toLowerCase() + '' + user.lastName.toLowerCase()).
                                    includes(search.toLowerCase())).
                                map(user => {
                                    return (<Users user={user} key={user._id} />)
                                }) : null
                            }
                        </ScrollView>
                    </ImageBackground>
                </View>
            }
        </View>
    )
}

const styles = StyleSheet.create({
    image: {
        width: "100%",
        flex: 1,
    },
    textInput: {
        marginTop: 10,
        color: "black",
        height: 30,
        borderColor: '#adbd00',
        borderWidth: 2,
        paddingLeft: 10,
        backgroundColor: "#e6e6e6",
        borderRadius: 30,
        marginHorizontal: 5
    },
    usersScroll: {
        alignContent: 'stretch',
        marginTop: 10,
    },
    centeredView: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 10,
    },
    container: {
        flex: 1,
        flexDirection: "row",
    }
});