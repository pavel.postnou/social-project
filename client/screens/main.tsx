import {
  Platform,
  Alert,
  StyleSheet,
  Text,
  View,
  ImageBackground,
  ScrollView,
  TouchableHighlight,
  Image,
} from "react-native";
import React, { useContext, useEffect, useState } from "react";
import Context from "../context/context";
import httpService from "../services/http.service";
import * as ImagePicker from "expo-image-picker";
import { Icon, Avatar } from "react-native-elements";
import { useFonts } from "expo-font";
import AppLoading from "expo-app-loading";
import { img } from "../constants/backgroundImage";
import PostItem from "../components/postItem";
import ModalPost from "../components/modalPost";
import { assert } from "console";

export default function MainScreen({ navigation }: { navigation: any }) {
  const context = useContext(Context);
  const [loading, setLoading] = useState(true);
  const [name, setName] = useState("");
  const [surname, setSurname] = useState("");
  const [modalVisible, setModalVisible] = useState(false);
  const [posts, setPosts] = useState<any[]>([]);
  const [ava, setAva] = useState<any>({ uri: context.userState.avatar });

  const myId = context.userState._id;
  const email = context.userState.email;

  useEffect(() => {
    setName(context.userState.firstName),
      setSurname(context.userState.lastName),
      setLoading(context.userState.loading);
    (async () => {
      const res = await httpService.getAllUserPostsById(myId);
      setPosts(res);
    })();
  }, [context.userState.loading]);

  useEffect(() => {
    (async () => {
      if (Platform.OS !== "web") {
        const { status } =
          await ImagePicker.requestMediaLibraryPermissionsAsync();
        if (status !== "granted") {
          alert("Sorry, we need camera roll permissions to make this work!");
        }
      }
    })();
  }, []);

  let [fontsLoaded] = useFonts({
    Dancing: require("../assets/fonts/DancingScript-Bold.ttf"),
    Rampart: require("../assets/fonts/RampartOne-Regular.ttf"),
    Permanent: require("../assets/fonts/PermanentMarker-Regular.ttf"),
    Staat: require("../assets/fonts/Staatliches-Regular.ttf"),
  });
  if (!fontsLoaded) {
    return <AppLoading />;
  }

  const pickAvatar = async () => {
    let result = await ImagePicker.launchImageLibraryAsync({
      mediaTypes: ImagePicker.MediaTypeOptions.All,
      allowsEditing: true,
      aspect: [4, 4],
      quality: 1,
    });
    if (!result.cancelled) {
      setAva({ uri: result.uri });
      const avatar = await httpService.changeAvatar(
        context.userState._id,
        result.uri
      );
      if (avatar) {
        Alert.alert("Avatar succesfully update");
      }
    }
  };
  const payStripe = () => {
    navigation.navigate("Payment");
  };
  const payArtPay = () => {
    navigation.navigate("ArtPay");
  };
  return (
    <View style={styles.container}>
      <ImageBackground source={img} resizeMode="cover" style={styles.image}>
        <View style={styles.infoView}>
          <Avatar
            title={name}
            size="large"
            onPress={() => pickAvatar()}
            rounded
            source={ava}
          />
          <View style={styles.info}>
            <Text style={styles.text}>{name}</Text>
            <Text style={styles.text}>{surname}</Text>
            <Text style={styles.text}>{email}</Text>
          </View>
            {loading ? (
              <View style={styles.container}>
                <View style={styles.pay}> 
                <Text style={styles.button}>Remove ads by</Text>
                <TouchableHighlight style={styles.imgPay} onPress={() => payStripe()}>
                <Image style={styles.imgPay} source={require("../assets/stripe.png")} />
                </TouchableHighlight>
                </View>
                <View style={styles.pay}>
                <Text style={styles.button}>Remove ads by</Text>
                <TouchableHighlight style={styles.imgPay} onPress={() => payArtPay()}>
                <Image style={styles.imgPay} source={require("../assets/artpay.jpg")} />
                </TouchableHighlight>
                </View>
              </View>
            ) : null}
        </View>
        <View style={styles.postView}>
          <ScrollView keyboardShouldPersistTaps="always">
            {posts.length > 0
              ? posts.map((item) => (
                  <PostItem
                    key={item._id}
                    item={item}
                    style={styles.postBlock}
                  />
                ))
              : null}
          </ScrollView>
        </View>
        {modalVisible ? (
          <ModalPost setModalVisible={setModalVisible} setPosts={setPosts} />
        ) : null}
      </ImageBackground>
      <View style={styles.icon}>
        <Icon
          name="add-circle"
          size={50}
          type="ion-icon"
          color="#36b5ff"
          onPress={() => setModalVisible(!modalVisible)}
        />
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  image: {
    width: "100%",
    flex: 1,
  },
  imgPay: {
    width: "100%",
    height: "100%",
    flex: 1,
    marginBottom:5
  },
  pay: {
    flex:1,
    flexDirection:"row",
    alignItems:"stretch",
    justifyContent:"space-between"
  },
  button: {
    backgroundColor: "darkgrey",
    color: "white",
    marginTop:7,
    height:20,
    marginRight:5
  },
  icon: {
    position: "absolute",
    right: 20,
    bottom: 20,
  },
  postBlock: {
    flex: 1,
    alignItems: "center",
    backgroundColor: "rgba(222, 222, 222, 0.5)",
    marginBottom: 10,
  },
  infoView: {
    flex: 1,
    backgroundColor: "rgba(222, 222, 222, 0.5)",
    padding: 10,
    flexDirection: "row"
  },
  postView: {
    flex: 6,
    marginTop: 10,
  },
  text: {
    color: "#1a1a1a",
    fontSize: 20,
    fontFamily: "Staat",
  },
  container: {
    flex: 2,
    justifyContent: "center",
    alignItems: "center",
  },
  info: {
    flex: 1,
    alignItems: "flex-start",
    justifyContent: "space-evenly",
    paddingLeft: 20,
  },
});
