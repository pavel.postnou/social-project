import { StyleSheet, Text, View, TextInput, TouchableHighlight, ImageBackground } from 'react-native';
import React, { useState, useContext } from 'react';
import httpService from '../services/http.service';
import Context from '../context/context';
import { img } from "../constants/backgroundImage"


export default function HomeScreen({ navigation }: { navigation: any }) {

  const [email, setEmail] = useState('');
  const [password, setPassword] = useState("");
  const context = useContext(Context)

  async function login() {
    const result = await httpService.logIn({ email, password });

    if (result) {
      const { firstName, lastName, email, avatar, _id, invitations } = result
      const invLength = invitations.length
      const loading = true
      await context.handleSetData({ firstName, lastName, email, avatar, _id, invLength, loading });
      setEmail(""),
        setPassword(""),
        navigation.navigate('Main')
    }
  }

  return (
    <View style={styles.container}>
      <ImageBackground source={img} resizeMode="cover" style={styles.image}>
        <Text style={styles.titleText}>{"Welcome to the\nSocial Network"}</Text>
        <View style={styles.login}>
          <TextInput style={styles.textInput} onChangeText={text => setEmail(text)} value={email} placeholder="email" />
          <TextInput style={styles.textInput} secureTextEntry={true} onChangeText={text => setPassword(text)} value={password} placeholder="password" />
          <TouchableHighlight
            style={styles.touchLogin}
            onPress={() => login()}>
            <Text style={styles.touchText}>LogIn</Text>
          </TouchableHighlight>
        </View>
        <TouchableHighlight
          style={styles.touch}
          onPress={() => navigation.navigate('Registration')}>
          <Text style={styles.touchText}>SignIn</Text>
        </TouchableHighlight>
      </ImageBackground>
    </View>
  );
}

const styles = StyleSheet.create({
  login: {
    alignSelf: "center",
    backgroundColor: "#303030",
    padding: 20,
    borderRadius: 30,
  },
  touch: {
    alignSelf: "center",
    backgroundColor: "#9e0000",
    padding: 10,
    borderRadius: 30,
    width: 200,
    marginBottom: 30,
    alignItems: "center",
  },
  touchLogin: {
    alignSelf: "center",
    backgroundColor: "#308aff",
    padding: 10,
    borderRadius: 30,
    width: 200,
    alignItems: "center",
  },
  touchText: {
    color: "white",
    fontSize: 20,
    fontWeight: "bold"
  },
  image: {
    width: "100%",
    flex: 1,
    alignItems: "center",
    justifyContent: 'space-between'
  },
  titleText: {
    marginTop: 20,
    color: "#090087",
    fontSize: 30,
    fontWeight: "bold",
    fontStyle: 'italic'
  },
  textInput: {
    color: "blue",
    height: 30,
    width: 200,
    borderColor: 'white',
    borderWidth: 2,
    marginBottom: 30,
    paddingLeft: 10,
    backgroundColor: "#a8a8a8",
    borderRadius: 30
  },
  container: {
    height: "100%",
    flex: 1,
    alignItems: "center",
  },
});