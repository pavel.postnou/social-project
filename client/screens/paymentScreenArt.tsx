import React, { useState } from "react";
import {
  View,
  StyleSheet,
  TextInput,
  Text,
  Button,
} from "react-native";

export default function ArtPay({ navigation }: { navigation: any }) {
  const API_URL = "http://192.168.1.46:3000";
  const [amount, setAmount] = useState("");

  const handlePayPress = async () => {
    const response = await fetch(`${API_URL}/payment/artpay/pay`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        paymentMethodType: "card",
        currency: "usd",
      }),
    });
    console.log(response);
  };
  const correct = (text: string) => {
    setAmount(text.replace(/\./g, "").replace(/(([0-9])([0-9])$)/g, ".$2$3"));
  };

  return (
    <View style={styles.container}>
      <Text>Для отключения рекламы требуется оплата</Text>
      <Text>Введите сумму сколько не жалко</Text>
      <TextInput
        style={styles.textInputCom}
        keyboardType="numeric"
        placeholder="0.00"
        onChangeText={(text) => correct(text)}
        value={amount}
      />
      <Button title="Оплатить" onPress={() => handlePayPress()}/>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
  },
  textInputCom: {
    color: "black",
    height: 30,
    borderColor: "#adbd00",
    width: "30%",
    borderWidth: 2,
    marginVertical: 10,
    marginLeft: 10,
    paddingLeft: 10,
    backgroundColor: "#e6e6e6",
    borderRadius: 30,
  },
});