import React, { useContext, useEffect, useState } from 'react';
import {StyleSheet, Text, View, ImageBackground, ScrollView } from 'react-native';
import httpService from '../services/http.service';
import Context from '../context/context';
import { img } from "../constants/backgroundImage"
import InvitationItem from '../components/invitationItem';
import FriendItem from '../components/friendItem';

export default function MainScreen({ navigation }: any) {
  const context = useContext(Context)
  const [invitations, setInvitations] = useState<any[]>([])
  const [friends, setFriends] = useState<any[]>([])

  useEffect(() => {
    (async () => {
      const myId = context.userState._id
      const res = await httpService.getAllFriendsById(myId);
      setInvitations(res.user.invitations)
      setFriends(res.user.friends)
    })()
  }, [context.userState.invLength])

  return (
    <View style={styles.usersView}>
      <ImageBackground source={img} resizeMode="cover" style={styles.image}>
        <View style={styles.block}>
          <Text style={styles.mainTitle}>Приглашения</Text>
          <ScrollView>
            {invitations.length > 0 ? invitations.map(item => (<InvitationItem invitation={item} key={item._id} />
            )) : null}
          </ScrollView>
        </View>
        <View style={styles.block}>
          <Text style={styles.mainTitle}>Друзья</Text>
          <ScrollView>
            {friends.length > 0 ? friends.map(item => (<FriendItem item={item} key={item._id} />
            )) : null}
          </ScrollView>
        </View>
      </ImageBackground>
    </View>
  )
}

const styles = StyleSheet.create({
  block: {
    flex: 1
  },
  mainTitle: {
    fontSize: 20,
    fontWeight: "bold",
    color: "blue",
    marginLeft: 10
  },
  usersView: {
    alignSelf: "center",
    width: "100%",
    height: "100%",
    backgroundColor: 'rgba(222, 222, 222,0.5)',
    borderRadius: 10
  },
  image: {
    width: "100%",
    flex: 1,
  }
});