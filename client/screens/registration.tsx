import { StyleSheet, TouchableHighlight, Text, View, TextInput, ImageBackground } from 'react-native';
import React, { useState } from 'react';
import httpService from '../services/http.service';
import { img } from "../constants/backgroundImage"

export default function RegScreen({ navigation }: { navigation: any }) {

  async function Registration() {
    const result = await httpService.signUp({ firstName, lastName, email, password });
    if (result) {
      navigation.navigate('Login')
    }
  }

  const [firstName, setFirstName] = useState('');
  const [lastName, setLastName] = useState('');
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState("");

  return (
    <View style={styles.container}>
      <ImageBackground source={img} resizeMode="cover" style={styles.image}>
        <Text style={{ ...styles.mainTitle }}>REGISTRATION</Text>
        <View style={styles.login}>
          <TextInput style={styles.textInput} onChangeText={text => setFirstName(text)} placeholder="First Name" value={firstName} />
          <TextInput style={styles.textInput} placeholder="Last Name" onChangeText={text => setLastName(text)} value={lastName} />
          <TextInput style={styles.textInput} secureTextEntry={true} placeholder="Password" onChangeText={text => setPassword(text)} value={password} />
          <TextInput style={styles.textInput} placeholder="Email" onChangeText={text => setEmail(text)} value={email} />
          <TouchableHighlight
            style={styles.touchLogin}
            onPress={() => Registration()}>
            <Text style={styles.touchText}>SignIn</Text>
          </TouchableHighlight>
        </View>
      </ImageBackground>
    </View>
  );
}

const styles = StyleSheet.create({

  image: {
    width: "100%",
    flex: 1,
    alignItems: "center",
  },
  login: {
    alignSelf: "center",
    backgroundColor: "#303030",
    padding: 20,
    borderRadius: 30
  },
  touchLogin: {
    alignSelf: "center",
    backgroundColor: "#308aff",
    padding: 10,
    borderRadius: 30,
    width: 200,
    alignItems: "center",
  },
  touchText: {
    color: "white",
    fontSize: 20,
    fontWeight: "bold"
  },
  mainTitle: {
    marginTop: 20,
    color: "#090087",
    fontSize: 30,
    fontWeight: "bold",
    fontStyle: 'italic'
  },
  textInput: {
    color: "black",
    height: 30,
    width: 200,
    borderColor: 'white',
    borderWidth: 2,
    marginBottom: 30,
    paddingLeft: 5,
    backgroundColor: "#a8a8a8",
    borderRadius: 30
  },
  container: {
    flex: 1,
    backgroundColor: '#d1d1d1',
    alignItems: "center",
    justifyContent: "space-evenly"
  }
});