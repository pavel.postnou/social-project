import React, { useContext, useEffect, useState, useRef } from 'react';
import { Alert, StyleSheet, Text, View, ImageBackground, ScrollView, TextInput } from 'react-native';
import { img } from "../constants/backgroundImage"
import Context from '../context/context';
import { socket } from '../services/http.service';
import httpService from '../services/http.service';
import { Avatar, Icon } from 'react-native-elements'
import ChatItem from '../components/chatItem';


export default function MainScreen() {
  const context = useContext(Context)
  const [message, setMessage] = useState("")
  const [chatId, setChatId] = useState("undef")
  const [messages, setMessages] = useState<any[]>([])
  const [friend, setFriend] = useState<any>()
  const scrollViewRef = useRef<any>();
  const [chat, setChat] = useState()

  const myId = context.userState._id;
  const friendId = context.userState.friendId;

  useEffect(() => {
    socket.connect(),
      (async () => {
        const res = await httpService.chatWithFriend(myId, friendId);
        if (res.chat != null) {
          setChatId(res?.chat._id)
          setMessages(res?.messages)
          const friendGet = await httpService.getAllFriendsById(friendId)
          setFriend(friendGet.user)
        }
        else {
          setChat(res.chat)
        }
        return () => socket.disconnect
      })()
  }, [friendId])

  useEffect(() => {
    scrollViewRef.current?.scrollToEnd({ animated: true })
  }, [messages.length])

  useEffect(() => {
    socket.on('tweet', ({ messages, oldChatId}: any) => {
      if (messages.chatId === oldChatId) {
        setMessages(messages.messages)
      }
      else { 
        const user = messages.messages[messages.messages.length - 1]
        Alert.alert("У вас новое сообщение от " + user.author.firstName + " " + user.author.lastName)
      }
    })
  }, [])

  const sendMessage = async (mes: string) => {
    socket.emit("sendMessage", { mes, myId, chatId })
  }

  return (
    <View style={styles.container}>
      <ImageBackground source={img} resizeMode="cover" style={styles.image}>
        <View style={styles.centeredView}>
          <Avatar size="small" rounded source={{ uri: friend?.avatar }} />
          <Text style={styles.title}> {friend?.firstName} {friend?.lastName}</Text>
        </View>
        <View style={styles.chatScreen}>
          <ScrollView ref={scrollViewRef}>
            {messages.length > 0 ? messages.map(item => (<ChatItem item={item} key={item._id} />
            )) : null}
          </ScrollView>
        </View>
        {chat == undefined ?
          <View style={styles.raw}>
            <TextInput style={styles.textInputCom} placeholder="message" onChangeText={text => setMessage(text)} value={message} />
            <View style={styles.send}>
              <Icon name='paper-plane' type='font-awesome' color='black' onPress={() => { sendMessage(message), setMessage("") }} />
            </View>
          </View>
          :
          null}
      </ImageBackground>
    </View>
  )
}

const styles = StyleSheet.create({

  image: {
    width: "100%",
    flex: 1,
  },

  chatScreen: {
    flex: 1
  },
  send: {
    marginTop: 10,
    marginLeft: 5
  },
  icon: {
    position: "absolute",
    right: 20,
    bottom: 20
  },
  title: {
    color: "darkblue",
    fontSize: 20,
    fontFamily: "Staat"
  },
  close: {
    position: "absolute",
    top: 0,
    right: 15
  },
  raw: {
    flexDirection: "row",
    marginBottom: 10
  },
  postBlock: {
    flex: 1,
    alignItems: 'center',
    backgroundColor: 'rgba(222, 222, 222, 0.5)',
    marginBottom: 10
  },
  textInput: {
    color: "black",
    height: 30,
    width: "100%",
    borderColor: '#adbd00',
    borderWidth: 2,
    paddingLeft: 10,
    backgroundColor: "#e6e6e6",
    borderRadius: 30,
    marginVertical: 20
  },
  textInputCom: {
    color: "black",
    height: 30,
    borderColor: '#adbd00',
    width: "88%",
    borderWidth: 2,
    marginTop: 10,
    marginLeft: 10,
    paddingLeft: 10,
    backgroundColor: "#e6e6e6",
    borderRadius: 30
  },
  message: {
    maxWidth: "75%",
    color: "black",
    borderColor: 'grey',
    width: "auto",
    borderWidth: 2,
    marginHorizontal: 5,
    paddingHorizontal: 5,
    backgroundColor: "#e6e6e6",
    borderRadius: 10
  },
  postTitle: {
    width: "100%",
    paddingLeft: 10,
    marginBottom: 10
  },
  commentsRow: {
    flexDirection: "row",
    width: "100%",
    paddingLeft: 10,
    height: 30,
    marginTop: 10
  },
  about: {
    color: "#1a1a1a",
    fontSize: 15,
    fontFamily: 'Staat'
  },
  logout: {
    marginLeft: 300,
    backgroundColor: "red",
    padding: 3,
    borderRadius: 30,
    width: "25%",
    alignItems: "center"
  },
  ava: {
    flex: 1
  },
  picture: {
    flexDirection: "row",
    justifyContent: "space-evenly"
  },
  infoView: {
    flex: 1,
    backgroundColor: 'rgba(222, 222, 222, 0.5)',
    padding: 10,
    flexDirection: "row"
  },
  centeredView: {
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: "row",
    backgroundColor: 'rgba(222, 222, 222, 0.5)',
  },

  modalView: {
    width: "90%",
    height: "70%",
    margin: 10,
    backgroundColor: 'white',
    borderRadius: 10,
    padding: 10,
    alignItems: "stretch",
    shadowColor: '#000',
    justifyContent: "space-evenly"
  },
  modalCom: {
    width: "100%",
    height: "85%",
    margin: 10,
    backgroundColor: 'white',
    borderRadius: 10,
    padding: 10,
    alignItems: "stretch",
    shadowColor: '#000',
    justifyContent: "space-evenly"
  },
  postView: {
    flex: 6,
    marginTop: 10
  },
  imgPost: {
    flex: 1,
    width: "100%",
    height: 220,
    resizeMode: "contain"
  },
  text: {
    color: "#1a1a1a",
    fontSize: 15,
    borderColor: "black"
  },
  container: {
    flex: 1,
    alignItems: "flex-start"
  },
  friendContainer: {
    maxWidth: "45%",
    marginLeft: 10,
    marginTop: 10,
    alignItems: "flex-start",
    flexDirection: "row"
  },
  myContainer: {
    marginRight: 10,
    marginTop: 10,
    alignItems: "flex-end",
  },
  myContainerOne: {
    marginRight: 10,
    marginTop: 10,
    alignItems: "flex-end",
    flexDirection: "row"
  },
  info: {
    flex: 2,
    alignItems: "flex-start",
    justifyContent: "space-evenly",
    paddingLeft: 20
  },
  touch: {
    alignSelf: "center",
    backgroundColor: "#308aff",
    padding: 5,
    borderRadius: 30,
    width: "95%",
    marginBottom: 10,
    alignItems: "center",
  },
  modalTouch: {
    alignSelf: "center",
    backgroundColor: "#308aff",
    padding: 5,
    borderRadius: 30,
    width: "95%",
    marginBottom: 10,
    alignItems: "center",
  },
  likes: {
    color: "#ff0000",
    fontSize: 20,
    fontWeight: "bold",
    marginHorizontal: 10
  },
  touchText: {
    color: "white",
    fontSize: 20,
    fontFamily: 'Staat'
  },
});
