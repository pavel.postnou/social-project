import * as yup from 'yup'

export const registrationSchema = yup.object().shape ({
    email: yup.string().email("Введите корректный email").required("Введите email"),
    password: yup.string().min(5).required("Введите пароль")
});
