import * as express from 'express'
const postRouter  = express.Router(); 
const postController = require("../controllers/postController");

postRouter.post("/create", postController.addNewPost);
postRouter.get("/:id", postController.getAllUserPostsById);
postRouter.put('/like',  postController.likePostById)
postRouter.put('/delete/:id',  postController.deletePost)

export {postRouter};
 