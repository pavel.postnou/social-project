import * as express from 'express'
const chatRouter = express.Router()

const chatController = require("../controllers/chatController");

chatRouter.put("/", chatController.getChat);
 
export {chatRouter};