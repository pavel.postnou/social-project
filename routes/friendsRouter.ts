import * as express from 'express'
const friendsRouter = express.Router()

const friendsController = require("../controllers/friendsController");

friendsRouter.get("/showAll", friendsController.showUsers);
friendsRouter.put("/askFriend", friendsController.askNewFriend);
friendsRouter.put("/del/declineFriend", friendsController.declineNewFriend);
friendsRouter.get("/friends/:id", friendsController.getUserInfo);
friendsRouter.put('/friends', friendsController.deleteFriend);
friendsRouter.put('/friends/add',  friendsController.addNewFriend);

 
export {friendsRouter};