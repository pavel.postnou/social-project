import * as express from 'express'
const authRouter = express.Router()

const authController = require("../controllers/authorizationController");

authRouter.post("/register", authController.regUser);
authRouter.post("/login", authController.loginUser);
 
export {authRouter};
