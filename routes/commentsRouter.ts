import * as express from 'express'
const commentsRouter  = express.Router()

const commentsController = require("../controllers/commentsController");

commentsRouter.post("/add", commentsController.addNewComment);
commentsRouter.get("/:id", commentsController.getComments);
commentsRouter.put("/delete/:id", commentsController.deleteComment);


 
export {commentsRouter};