import * as express from 'express'
const paymentRouter = express.Router()

const paymentController = require("../controllers/paymentController");
const artpayController = require("../controllers/artpayController")

paymentRouter.post("/checkout", paymentController.pay);
paymentRouter.post("/checkout/pay/subscribe", paymentController.subscribe);
paymentRouter.post("/artpay/pay", artpayController.artPay);
 
export {paymentRouter};
