import * as express from 'express'
const profileRouter = express.Router()

const profileController = require("../controllers/profileController");

profileRouter.put("/change", profileController.changeAvatar);

export {profileRouter};